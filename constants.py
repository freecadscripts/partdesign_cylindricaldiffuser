from math import tan, radians

### Document Name
DOC_NAME_0 = 'PartDesign_CylindricalDiffuser'


### Label
BODY_0_LABEL = 'Body'

SKETCH_0_LABEL = 'Sketch_taper'
SKETCH_1_LABEL = 'Sketch_inlet_side'
SKETCH_2_LABEL = 'Sketch_outlet_side'
SKETCH_3_LABEL = 'Sketch_triming'

REVOLUTION_0_LABEL = 'Revolution'

PAD_0_LABEL = 'Pad_inlet_side'
PAD_1_LABEL = 'Pad_outlet_side'

POCKET_0_LABEL = 'Pocket'

X_AXIS = -1
Y_AXIS = -2


### Geometry (mm, deg)
DIAMETER_1 = 100.0
DIAMETER_2 = 1.5 * DIAMETER_1

SMALL_LENGTH = 5.0
STRAIGHT_LENGTH_INLET = 0.75 * DIAMETER_1 - SMALL_LENGTH
STRAIGHT_LENGTH_OUTLET = 2.0 * DIAMETER_1 - SMALL_LENGTH

TAPER_ANGLE = 45.0
