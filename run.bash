#!/bin/bash

# set command name ('freecadcmd' for deb package and 'FreeCADCmd' for rpm package)
if [ -d /etc/apt ]; then
    freecadcmd='freecadcmd'
elif [ -d /etc/dnf ]; then
    freecadcmd='FreeCADCmd'
elif [ -d /etc/zypp ]; then
    freecadcmd='FreeCADCmd'
else
    freecadcmd='FreeCADCmd'
fi

$freecadcmd script.py
