import os
import numpy as np
import FreeCAD as App
import Part
import Sketcher

import constants as c


def create_tapered_cylinder(body_0):
    sketch_0 = body_0.newObject('Sketcher::SketchObject', c.SKETCH_0_LABEL)
    sketch_0.Support = (doc_0.getObject('XY_Plane'), [''])
    sketch_0.MapMode = 'FlatFace'

    geo_list_0 = []
    # index 0 ~ 4
    geo_list_0.append(Part.LineSegment(App.Vector(0.0, 25.0, 0.0), App.Vector(-5.0, 25.0, 0.0)))
    geo_list_0.append(Part.LineSegment(App.Vector(-5.0, 25.0, 0.0), App.Vector(-5.0, 0.0, 0.0)))
    geo_list_0.append(Part.LineSegment(App.Vector(-5.0, 0.0, 0.0), App.Vector(100.0, 0.0, 0.0)))
    geo_list_0.append(Part.LineSegment(App.Vector(100.0, 0.0, 0.0), App.Vector(100.0, 50.0, 0.0)))
    geo_list_0.append(Part.LineSegment(App.Vector(100.0, 50.0, 0.0), App.Vector(95.0, 50.0, 0.0)))
    # index 5
    geo_list_0.append(Part.LineSegment(App.Vector(95.0, 50.0, 0.0), App.Vector(0.0, 25.0, 0.0)))

    geo_0 = sketch_0.addGeometry(geo_list_0, False)

    con_list_0 = []
    # index 0 ~ 4
    con_list_0.append(Sketcher.Constraint('Coincident', geo_0[1], 1, geo_0[0], 2))
    con_list_0.append(Sketcher.Constraint('Coincident', geo_0[2], 1, geo_0[1], 2))
    con_list_0.append(Sketcher.Constraint('Coincident', geo_0[3], 1, geo_0[2], 2))
    con_list_0.append(Sketcher.Constraint('Coincident', geo_0[4], 1, geo_0[3], 2))
    con_list_0.append(Sketcher.Constraint('Coincident', geo_0[5], 1, geo_0[4], 2))
    # index 5 ~ 9
    con_list_0.append(Sketcher.Constraint('Coincident', geo_0[0], 1, geo_0[5], 2))
    con_list_0.append(Sketcher.Constraint('Horizontal', geo_0[0]))
    con_list_0.append(Sketcher.Constraint('Vertical', geo_0[1]))
    con_list_0.append(Sketcher.Constraint('Horizontal', geo_0[2]))
    con_list_0.append(Sketcher.Constraint('Vertical', geo_0[3]))
    # index 10 ~ 14
    con_list_0.append(Sketcher.Constraint('Horizontal', geo_0[4]))
    con_list_0.append(Sketcher.Constraint('PointOnObject', geo_0[0], 1, c.Y_AXIS))
    con_list_0.append(Sketcher.Constraint('PointOnObject', geo_0[1], 2, c.X_AXIS))
    con_list_0.append(Sketcher.Constraint('DistanceY', geo_0[1], 2, geo_0[1], 1, 25.0))
    con_list_0.append(Sketcher.Constraint('DistanceY', geo_0[3], 1, geo_0[3], 2, 50.0))
    # index 15 ~ 17
    con_list_0.append(Sketcher.Constraint('DistanceX', geo_0[0], 2, geo_0[0], 1, 5.0))
    con_list_0.append(Sketcher.Constraint('DistanceX', geo_0[4], 2, geo_0[4], 1, 5.0))
    con_list_0.append(Sketcher.Constraint('Angle', geo_0[2], 1, geo_0[5], 2, 0.31416))  # rad

    con_0 = sketch_0.addConstraint(con_list_0)

    # note: constraint of DIAMETER_2 MUST BE defined before that of DIAMETER_1
    #       because DIAMETER_2 > DIAMETER_1. (Or the sketch solver may fail to update geometry.)
    sketch_0.setDatum(con_0[14], App.Units.Quantity(str(0.5 * c.DIAMETER_2) + ' mm'))
    sketch_0.setDatum(con_0[13], App.Units.Quantity(str(0.5 * c.DIAMETER_1) + ' mm'))
    sketch_0.setDatum(con_0[15], App.Units.Quantity(str(c.SMALL_LENGTH) + ' mm'))
    sketch_0.setDatum(con_0[16], App.Units.Quantity(str(c.SMALL_LENGTH) + ' mm'))
    sketch_0.setDatum(con_0[17], App.Units.Quantity(str(0.5 * c.TAPER_ANGLE) + ' deg'))

    del geo_list_0, con_list_0
    doc_0.recompute()

    # Begin command PartDesign_Revolution
    revolution_0 = body_0.newObject('PartDesign::Revolution', c.REVOLUTION_0_LABEL)
    revolution_0.Profile = sketch_0
    revolution_0.ReferenceAxis = (doc_0.getObject('X_Axis'), [''])
    revolution_0.Angle = 360.0

    doc_0.recompute()

    return revolution_0


def get_faces_on_which_sketch_is_created(revolution_0):
    faces_rev_0 = revolution_0.Shape.Faces

    x_of_faces = []
    for i in range(len(faces_rev_0)):
        face = faces_rev_0[i]
        x_of_faces.append(face.CenterOfMass.x)

    INDEX_MIN_X = np.argmin(x_of_faces)
    INDEX_MAX_X = np.argmax(x_of_faces)

    return INDEX_MIN_X, INDEX_MAX_X


def create_pad_on_revolution(body_0, SKETCH_LABEL, FACE_INDEX, DIAMETER, PAD_LABEL, PAD_LENGTH):
    revolution_0 = body_0.getObject(c.REVOLUTION_0_LABEL)
    # make sketch
    sketch = body_0.newObject('Sketcher::SketchObject', SKETCH_LABEL)
    # note: the name list of SubObject begins from 'Face1', not 'Face0'
    sketch.Support = (revolution_0, 'Face' + str(FACE_INDEX + 1))
    sketch.MapMode = 'FlatFace'

    geo_list = []
    # index 0
    geo_list.append(Part.Circle(App.Vector(0.0, 0.0, 0.0), App.Vector(0, 0, 1), 50.0))

    geo = sketch.addGeometry(geo_list, False)

    con_list = []
    # index 0 ~ 1
    con_list.append(Sketcher.Constraint('Coincident', geo[0], 3, c.X_AXIS, 1))
    # note: 'c.X_AXIS, 1' might indicates the cross point with sketch surface?
    con_list.append(Sketcher.Constraint('Diameter', geo[0], 50.0))

    con = sketch.addConstraint(con_list)

    sketch.setDatum(con[1], App.Units.Quantity(str(DIAMETER) + ' mm'))

    del geo_list, con_list
    doc_0.recompute()

    # Begin command PartDesign_Pad
    pad = body_0.newObject('PartDesign::Pad', PAD_LABEL)
    pad.Profile = sketch
    pad.Length = PAD_LENGTH
    pad.ReferenceAxis = (sketch, ['N_Axis'])

    doc_0.recompute()

    return


def trim_three_fourths_of_whole_body(body_0):
    # Remove 3/4 of the whole body (to reduce computational cost in CFD)
    sketch_3 = body_0.newObject('Sketcher::SketchObject', c.SKETCH_3_LABEL)
    sketch_3.Support = (doc_0.getObject('YZ_Plane'), [''])
    sketch_3.MapMode = 'FlatFace'

    geo_list_3 = []
    # index 0 ~ 2
    geo_list_3.append(Part.Circle(App.Vector(
        0.0, 0.0, 0.0), App.Vector(0, 0, 1), 150.0))
    geo_list_3.append(Part.LineSegment(App.Vector(
        0.0, 0.0, 0), App.Vector(0.0, 150.0, 0.0)))
    geo_list_3.append(Part.LineSegment(App.Vector(
        0.0, 0.0, 0), App.Vector(150.0, 0.0, 0.0)))

    geo_3 = sketch_3.addGeometry(geo_list_3, False)

    con_list_3 = []
    # index 0 ~ 4
    con_list_3.append(Sketcher.Constraint('Coincident', geo_3[0], 3, c.X_AXIS, 1))
    con_list_3.append(Sketcher.Constraint('Coincident', geo_3[1], 1, geo_3[0], 3))
    con_list_3.append(Sketcher.Constraint('PointOnObject', geo_3[1], 2, geo_3[0]))
    con_list_3.append(Sketcher.Constraint('Vertical', geo_3[1]))
    con_list_3.append(Sketcher.Constraint('Coincident', geo_3[2], 1, geo_3[0], 3))
    # index 5 ~ 7
    con_list_3.append(Sketcher.Constraint('PointOnObject', geo_3[2], 2, geo_3[0]))
    con_list_3.append(Sketcher.Constraint('Horizontal', geo_3[2]))
    con_list_3.append(Sketcher.Constraint('Radius', geo_3[0], 50.0))

    con_3 = sketch_3.addConstraint(con_list_3)

    del geo_list_3, con_list_3

    sketch_3.setDatum(con_3[7], App.Units.Quantity(str(0.5 * c.DIAMETER_2) + ' mm'))
    # a point on the first quadrant of the circle (using Pythagorean theorem)
    sketch_3.trim(geo_3[0], App.Vector(0.4 * c.DIAMETER_2, 0.3 * c.DIAMETER_2, 0.0))

    doc_0.recompute()

    # Begin command PartDesign_Pocket
    pocket_0 = body_0.newObject('PartDesign::Pocket', c.POCKET_0_LABEL)
    pocket_0.Profile = sketch_3
    doc_0.recompute()
    pocket_0.ReferenceAxis = (sketch_3, ['N_Axis'])
    pocket_0.AlongSketchNormal = 1
    pocket_0.Type = 1  # boring through
    pocket_0.Midplane = 1  # both side

    doc_0.recompute()
    # End command PartDesign_Pocket
    return


### MAIN
doc_0 = App.newDocument(c.DOC_NAME_0)
body_0 = doc_0.addObject('PartDesign::Body', c.BODY_0_LABEL)

revolution_0 = create_tapered_cylinder(body_0)

INDEX_MIN_X, INDEX_MAX_X = get_faces_on_which_sketch_is_created(revolution_0)

create_pad_on_revolution(body_0, c.SKETCH_1_LABEL, INDEX_MIN_X, c.DIAMETER_1, c.PAD_0_LABEL, c.STRAIGHT_LENGTH_INLET)

create_pad_on_revolution(body_0, c.SKETCH_2_LABEL, INDEX_MAX_X, c.DIAMETER_2, c.PAD_1_LABEL, c.STRAIGHT_LENGTH_OUTLET)

# trim_three_fourths_of_whole_body(body_0)

pwd = os.getcwd()
file_path_0 = pwd + "/" + c.DOC_NAME_0 + ".FCStd"
doc_0.saveAs(file_path_0)
