# PartDesign_CylindricalDiffuser



## Description

KORE GA...

![](readme_images/1_korega.png)

KOU NATTE...

![](readme_images/2_kounatte.png)

KOU JA.

![](readme_images/3_kouja.png)

## Requirement

* Linux (Debian, RHEL, SUSE based distro is OK. the others are out of consideration)
* FreeCAD (v0.20 is used)
* OpenFOAM (v2206 is used)


## How To Use

* download this project folder and extruct it in some suitable place.
* open terminal in that folder and type ```./run.bash``` and geometry files will be created.
* to clean up the folder, type ```./clean.bash```


## Derived into:

* https://gitlab.com/freecadscripts/exportstl_cylindricaldiffuser


## Reference

* https://wiki.freecadweb.org/Scripts
* https://wiki.freecadweb.org/Sketcher_scripting
